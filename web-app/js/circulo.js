// Definição das Constantes
const SRU = 5;
const COR_PLANO = 0x000000;
const COR_SENO = 0xffff00; // Amarelo
const COR_COSSENO = 0xff0000; // Vermelho
const COR_TANGENTE = 0x00cc00; // Verde
const OPACIDADE_PLANO = 0.6;
const DISTANCIA_MARCADORES = 1;
const RAIO_CIRCULO = 4;
const SEGMENTOS_CIRCULO = 90;
const ANGULO_INICIAL_QUADRANTE = 0;

// Definição da Classe do Circulo Trigonometrico
function CirculoTrigonometrico()
{
	var circulo,
		camera,
		selecao,
		plane,
		quadrante,
		borda_circulo,
		objeto_selecionado,
		linha_seno,
		linha_seno2,
		linha_seno3,
		linha_seno4,
		linha_seno5,
		linha_cosseno,
		linha_cosseno2,
		linha_cosseno3,
		linha_cosseno4,
		linha_cosseno5,
		linha_tangente,
		linha_pont_tangente,
		linha_pont_tangente2,
		linha_pont_tangente3,
		angulo;

	var width, height;

	var mousePressed = false;

	// Desenho do Plano Cartesiano
	this.desenha_plano_cartesiano = function( scene )
	{
		var material_linha_plano = new THREE.LineBasicMaterial({ color: COR_PLANO, transparent: true, opacity: OPACIDADE_PLANO });

		var geometry_linhaX = new THREE.Geometry();

		geometry_linhaX.vertices.push(
			new THREE.Vector3( -SRU, 0, 0 ),
			new THREE.Vector3( +SRU, 0, 0 )
		);

		var geometry_linhaY = new THREE.Geometry();

		geometry_linhaY.vertices.push(
			new THREE.Vector3( 0, -SRU, 0 ),
			new THREE.Vector3( 0, +SRU, 0 )
		);

		var linhaX = new THREE.Line( geometry_linhaX, material_linha_plano );
		var linhaY = new THREE.Line( geometry_linhaY, material_linha_plano );

		scene.add( linhaX );
		scene.add( linhaY );
	},

	//Desenha uma linha
	this.desenha_linha = function( scene, cor, pontoAx, pontoAy, pontoBx, pontoBy )
	{
		var material_linha_plano = new THREE.LineBasicMaterial({ color: cor, transparent: true, opacity: OPACIDADE_PLANO });

		var geometry_linha = new THREE.Geometry();

		geometry_linha.vertices.push(
			new THREE.Vector3( pontoAx, pontoAy, 0 ),
			new THREE.Vector3( pontoBx, pontoBy, 0 )
		);

		var linha = new THREE.Line( geometry_linha, material_linha_plano );

		scene.add( linha );

		return linha;
	},

	//Desenha uma linha pontilhada
	this.desenha_linha_pontilhada = function( scene, cor, pontoAx, pontoAy, pontoBx, pontoBy )
	{
		var material_linha_plano = new THREE.LineDashedMaterial({color: cor, dashSize: 0.1, gapSize: 0.1});

		var geometry_linha = new THREE.Geometry();

		geometry_linha.vertices.push(
			new THREE.Vector3( pontoAx, pontoAy, 0 ),
			new THREE.Vector3( pontoBx, pontoBy, 0 )
		);

		var linha_pontilhada = new THREE.Line( geometry_linha, material_linha_plano );

		scene.add( linha_pontilhada );

		return linha_pontilhada;
	},

	// Desenho do Fundo do Círculo
	this.desenha_fundo_circulo = function( scene )
	{
		var geometry = new THREE.CircleGeometry( RAIO_CIRCULO, SEGMENTOS_CIRCULO );
		var material = new THREE.MeshBasicMaterial( { color: 0x006699 } );
		var objeto_grafico = new THREE.Mesh( geometry, material );

		scene.add( objeto_grafico );

		var geometry_borda = new THREE.CircleGeometry( RAIO_CIRCULO, SEGMENTOS_CIRCULO );
		var material_borda = new THREE.LineBasicMaterial( { color: 0x000000 } );
		borda_circulo = new THREE.Line( geometry_borda, material_borda );

		scene.add( borda_circulo );
	},

	// Desenho da Fatia do Círculo
	this.desenha_quadrante = function( scene )
	{
		quadrante = new QuadranteCirculo(RAIO_CIRCULO, ANGULO_INICIAL_QUADRANTE);

		scene.add(quadrante.mesh);
	},

	// Desenhar marcadores do eixo X
	this.desenha_marcadores_eixos = function( scene )
	{
		var adiciona_marcador = function(xA, yA, xB, yB)
		{
			var line_material = new THREE.LineBasicMaterial( { color: COR_PLANO, linewidth: 3, transparent: true, opacity: OPACIDADE_PLANO } );
			var line_geometry = new THREE.Geometry();

			line_geometry.vertices.push(
				new THREE.Vector3( xA, yA, 0 ),
				new THREE.Vector3( xB, yB, 0 )
			);

			var line = new THREE.Line( line_geometry, line_material );

			scene.add( line );
		}

		for (var i = -SRU; i <= +SRU; i = i + DISTANCIA_MARCADORES)
		{
			adiciona_marcador(i, -0.1, i, +0.1);
			adiciona_marcador(-0.1, i, +0.1, i);
		}
	},

	//Desenha a numeração dos eixos
	this.desenha_numeracao_eixos = function( scene ) {
		var laranja = 0xff3300;
		this.desenha_texto(scene, "0.5", 0.2, laranja, -0.5, 1.9);
		this.desenha_texto(scene, "1", 0.2, laranja, -0.25, 3.9);

		this.desenha_texto(scene, "0.5", 0.2, laranja, -0.5, -2.1);
		this.desenha_texto(scene, "1", 0.2, laranja, -0.25, -4.1);

		this.desenha_texto(scene, "0.5", 0.2, laranja, 1.8, -0.4);
		this.desenha_texto(scene, "1", 0.2, laranja, 3.9, -0.4);

		this.desenha_texto(scene, "0.5", 0.2, laranja, -2.2, -0.4);
		this.desenha_texto(scene, "1", 0.2, laranja, -4.1, -0.4);
	}

	// Desenho do Círculo de Seleção
	this.desenha_circulo_selecao = function( scene )
	{
		var geometry = new THREE.CircleGeometry( 0.2, SEGMENTOS_CIRCULO );
		var material = new THREE.MeshBasicMaterial( { color: 0x000000 } );
		selecao = new THREE.Mesh( geometry, material );

		scene.add( selecao );
	},

	this.desenha_texto = function ( scene, texto, tamanho, cor, x, y )
	{
		var geo_texto = new THREE.TextGeometry(texto, { size: tamanho, height: 0 });
		var mat_texto = new THREE.MeshBasicMaterial( { color: cor } );
		var obj_texto = new THREE.Mesh( geo_texto, mat_texto );

		obj_texto.position.x = x;
		obj_texto.position.y = y;

		scene.add( obj_texto );
	},
	this.verifica_clique_objeto = function(e, objeto_grafico)
	{
		// pega a posição do mouse
		var mouseX = e.offsetX || e.clientX;
		var mouseY = e.offsetY || e.clientY;

		mouseX = (mouseX / width) * 2 - 1;
		mouseY = -(mouseY / height) * 2 + 1;

		return circulo.verifica_objeto_posicao(mouseX, mouseY, objeto_grafico);
	},

	this.verifica_toque_objeto = function(touch, objeto_grafico)
	{
		// pega a posição do mouse
		var touchX = touch.pageX;
		var touchY = touch.pageY;

		touchX =  (touchX / width) * 2 - 1;
		touchY = -(touchY / height) * 2 + 1;

		return circulo.verifica_objeto_posicao(touchX, touchY, objeto_grafico);
	},

	this.verifica_objeto_posicao = function(x, y, objeto_grafico)
	{
		// cria um novo vetor nas coordenadas
		// corretas
		var vector = new THREE.Vector3(x, y, 0.5);

	    vector.unproject( camera );

		// criar um raio com base na posição atual da câmera
		// assim sabemos a face que está virada para a câmera no momento
		var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());

		intersects = raycaster.intersectObject(objeto_grafico);

		// se o raio tiver intersecção com a superfície
		// executa a função para distorcer
		if (intersects.length)
		{
			return intersects[0].point;
		}

		return null;
	},

	this.atualizaTrignometria = function(angulo_ponto, ponto_circulo, sin, cos, tan) {
		circulo.angulo = angulo_ponto;
		document.getElementById("angulo").innerHTML = "Ângulo: " + angulo_ponto;
		document.getElementById("seno").innerHTML = "Seno: " + parseFloat(sin.toFixed(3));
		document.getElementById("cosseno").innerHTML = "Cosseno: " + parseFloat(cos.toFixed(3));
		if (angulo_ponto == 90 || angulo_ponto == 270) {
			// tangente de 90 ou 270 graus tende ao infinito
			document.getElementById("tangente").innerHTML = "Tangente: -";
		} else {
			document.getElementById("tangente").innerHTML = "Tangente: " + parseFloat(tan.toFixed(3));
		}

		//Atualiza linhaa seno
		circulo.linha_seno.geometry.vertices[1].y = ponto_circulo.y;
		circulo.linha_seno.geometry.verticesNeedUpdate = true;

		circulo.linha_seno2.geometry.vertices[1].y = ponto_circulo.y;
		circulo.linha_seno2.geometry.verticesNeedUpdate = true;

		circulo.linha_seno3.geometry.vertices[1].y = ponto_circulo.y;
		circulo.linha_seno3.geometry.verticesNeedUpdate = true;

		circulo.linha_seno4.geometry.vertices[1].y = ponto_circulo.y;
		circulo.linha_seno4.geometry.verticesNeedUpdate = true;

		circulo.linha_seno5.geometry.vertices[1].y = ponto_circulo.y;
		circulo.linha_seno5.geometry.verticesNeedUpdate = true;

		//Atualiza linhas cosseno
		circulo.linha_cosseno.geometry.vertices[1].x = ponto_circulo.x;
		circulo.linha_cosseno.geometry.verticesNeedUpdate = true;

		circulo.linha_cosseno2.geometry.vertices[1].x = ponto_circulo.x;
		circulo.linha_cosseno2.geometry.verticesNeedUpdate = true;

		circulo.linha_cosseno3.geometry.vertices[1].x = ponto_circulo.x;
		circulo.linha_cosseno3.geometry.verticesNeedUpdate = true;

		circulo.linha_cosseno4.geometry.vertices[1].x = ponto_circulo.x;
		circulo.linha_cosseno4.geometry.verticesNeedUpdate = true;

		circulo.linha_cosseno5.geometry.vertices[1].x = ponto_circulo.x;
		circulo.linha_cosseno5.geometry.verticesNeedUpdate = true;

		// Atuliza linha tangente
		circulo.linha_pont_tangente.geometry.vertices[1].y = tan * RAIO_CIRCULO;
		circulo.linha_pont_tangente.geometry.verticesNeedUpdate = true;

		circulo.linha_pont_tangente2.geometry.vertices[1].y = tan * RAIO_CIRCULO + 0.01;
		circulo.linha_pont_tangente2.geometry.verticesNeedUpdate = true;

		circulo.linha_pont_tangente3.geometry.vertices[1].y = tan * RAIO_CIRCULO -0.01;
		circulo.linha_pont_tangente3.geometry.verticesNeedUpdate = true;
	}

	// criando um objeto com todos os callbacks
	this.callbacks =
	{
		/**
		* Executado quando a janela for redimensionada
		*/
		onResize: function()
		{

			//recalculando o aspect ratio com base no novo tamanho da janela
			//camera.aspect = window.innerWidth / window.innerHeight;

			//atualizando a matriz de projeção da câmera
			//camera.updateProjectionMatrix();

			//atualizando as dimensões do renderizador
			//renderer.setSize( window.innerWidth, window.innerHeight );
		},

		/**
		* Executado quando o usuário fizer um clique com o mouse
		*
		* @param {Evento} e Recebe como parâmetro, o objeto do evento disparado
		*/
		onMouseDown: function(e)
		{
			//atualizando a variável global que indica se o mouse ainda está pressionado ou não
			mousePressed = true;
			objeto_selecionado = null;

			if (circulo.verifica_clique_objeto(e, selecao))
			{
				objeto_selecionado = selecao;
			}
		},

		/**
		* Executado quando o mouse se mover
		*
		* @param {Evento} e Recebe como parâmetro, o objeto do evento disparado
		*/
		onMouseMove: function(e)
		{
			e.preventDefault();

			if (mousePressed)
			{
				if (objeto_selecionado)
				{
					var ponto = circulo.verifica_clique_objeto(e, plane);
					var angulo_ponto = Math.angulo(ponto.x, ponto.y);
					var resto = angulo_ponto % 5;
					angulo_ponto -= resto;
										
					var angulo_radianos = Math.radians(angulo_ponto);

					var sin = Math.sin(angulo_radianos);
					var cos = Math.cos(angulo_radianos);
					var tan = Math.tan(angulo_radianos);

					var ponto_circulo = vectorDeRaioAngulo(angulo_ponto, RAIO_CIRCULO);
					selecao.position.copy(ponto_circulo);
					quadrante.atualizar(angulo_ponto);

					circulo.atualizaTrignometria(angulo_ponto, ponto_circulo, sin, cos, tan);
				}
			}
		},

		/**
		* Executado quando o usuário soltar o clique do mouse
		*/
		onMouseUp: function()
		{
			//atualizando a variável global que indica se o mouse ainda está pressionado ou não
			mousePressed = false;
			objeto_selecionado = null;
		},

		/**
		* Evitando que o usuário selecione o conteúdo html
		* Como por padrão, ao clicar segurar e arrastar o navegador exibe o cursor de seleção
		* Retornamos false para não permitir que nenhum conteúdo seja selecionado, prejudicando assim a experiência do usuário
		*/
		onSelectStart: function()
		{
			return false;
		},

		onTouchStart: function( e )
		{
			if ( e.touches.length === 1 )
			{
				e.preventDefault();

				var touch = e.touches[0];

				if (circulo.verifica_toque_objeto(touch, selecao))
				{
					objeto_selecionado = selecao;
				}
			}
		},

		onTouchMove: function( e )
		{
			if ( e.touches.length === 1 )
			{
				e.preventDefault();

				var touch = e.touches[0];

				if (objeto_selecionado)
				{
					var ponto = circulo.verifica_toque_objeto(touch, plane);
					var angulo_ponto = Math.angulo(ponto.x, ponto.y);

					var resto = angulo_ponto % 5;
					angulo_ponto -= resto;
					
					var angulo_radianos = Math.radians(angulo_ponto);

					var sin = Math.sin(angulo_radianos);
					var cos = Math.cos(angulo_radianos);
					var tan = Math.tan(angulo_radianos);

					var ponto_circulo = vectorDeRaioAngulo(angulo_ponto, RAIO_CIRCULO);
					selecao.position.copy(ponto_circulo);
					quadrante.atualizar(angulo_ponto);

					circulo.atualizaTrignometria(angulo_ponto, ponto_circulo, sin, cos, tan);
				}
			}
		},

		onTouchEnd: function( e )
		{
			objeto_selecionado = null;
		}
	},

	this.desenhar = function( container )
	{
		/*
		var width = window.innerWidth;
		var height = window.innerHeight;
		/*/
		width = container.clientWidth;
		height = container.clientHeight;
		//*/

		camera = new THREE.PerspectiveCamera( 100, width / height, 1, 10000 );

		var scene = new THREE.Scene();

		var renderer = new THREE.WebGLRenderer({ antialias: true });

		renderer.setSize( width, height );
		renderer.setClearColor( 0xf0f0f0 );
		//renderer.setPixelRatio( window.devicePixelRatio );

		container.appendChild( renderer.domElement );

		var plane_geometry = new THREE.PlaneGeometry( width, height, 4, 4 );
		var plane_material = new THREE.MeshBasicMaterial( {color: 0xDDDDDD, side: THREE.DoubleSide} );

		plane = new THREE.Mesh( plane_geometry, plane_material );

		scene.add( plane );

		this.desenha_fundo_circulo( scene );
		this.desenha_quadrante( scene );
		this.desenha_plano_cartesiano( scene );
		this.desenha_marcadores_eixos( scene );
		this.desenha_numeracao_eixos( scene );
		this.desenha_circulo_selecao( scene );

		//this.desenha_texto( scene, "Angulo: " + angulo_fatia, 0.5, 0xFF0000, 6, 3 );

		this.desenha_texto( scene, "0", 0.3, 0xFF0000, 5.1, -0.15 );
		this.desenha_texto( scene, "o", 0.15, 0xFF0000, 5.34, 0.07 );

		this.desenha_texto( scene, "90", 0.3, 0xFF0000, -0.24, 5.1 );
		this.desenha_texto( scene, "o", 0.15, 0xFF0000, 0.25, 5.3 );

		this.desenha_texto( scene, "180", 0.3, 0xFF0000, -5.9, -0.15 );
		this.desenha_texto( scene, "o", 0.15, 0xFF0000, -5.19, 0.07 );

		this.desenha_texto( scene, "270", 0.3, 0xFF0000, -0.35, -5.4 );
		this.desenha_texto( scene, "o", 0.15, 0xFF0000, 0.38, -5.2 );

		//Seno
		var seno_zero = Math.sin(0);
		this.linha_seno = this.desenha_linha(scene, COR_SENO, 0, 0, 0, seno_zero);
		this.linha_seno2 = this.desenha_linha(scene, COR_SENO, 0.01, 0, 0.01, seno_zero);
		this.linha_seno3 = this.desenha_linha(scene, COR_SENO, 0.02, 0, 0.02, seno_zero);
		this.linha_seno4 = this.desenha_linha(scene, COR_SENO, -0.01, 0, -0.01, seno_zero);
		this.linha_seno5 = this.desenha_linha(scene, COR_SENO, -0.02, 0, -0.02, seno_zero);

		// Cosseno
		this.linha_cosseno = this.desenha_linha(scene, COR_COSSENO, 0, 0, RAIO_CIRCULO, 0);
		this.linha_cosseno2 = this.desenha_linha(scene, COR_COSSENO, 0, 0.01, RAIO_CIRCULO, 0.01);
		this.linha_cosseno3 = this.desenha_linha(scene, COR_COSSENO, 0, 0.02, RAIO_CIRCULO, 0.02);
		this.linha_cosseno4 = this.desenha_linha(scene, COR_COSSENO, 0, -0.01, RAIO_CIRCULO, -0.01);
		this.linha_cosseno5 = this.desenha_linha(scene, COR_COSSENO, 0, -0.02, RAIO_CIRCULO, -0.02);

		//Tangente
		this.linha_tangente = this.desenha_linha(scene, COR_TANGENTE, RAIO_CIRCULO, 100, RAIO_CIRCULO, -100);
		this.linha_pont_tangente = this.desenha_linha_pontilhada(scene, COR_TANGENTE, 0, 0, RAIO_CIRCULO, Math.tan(0));
		this.linha_pont_tangente2 = this.desenha_linha_pontilhada(scene, COR_TANGENTE, 0, 0.01, RAIO_CIRCULO, Math.tan(0) + 0.01);
		this.linha_pont_tangente3 = this.desenha_linha_pontilhada(scene, COR_TANGENTE, 0, -0.01, RAIO_CIRCULO, Math.tan(0) - 0.01);

		camera.position.z = 5;

		circulo = this;

		selecao.position.copy(vectorDeRaioAngulo(ANGULO_INICIAL_QUADRANTE, RAIO_CIRCULO));

		var render = function ()
		{
			requestAnimationFrame( render );

			renderer.render(scene, camera);
		};

		render();

		// disparando os callbacks quando...
		window.addEventListener('resize', this.callbacks.onResize, false); //redimensionar a janela
		window.addEventListener('mousedown', this.callbacks.onMouseDown, false); //clicar
		window.addEventListener('mousemove', this.callbacks.onMouseMove, false); //mover o mouse
		window.addEventListener('mouseup', this.callbacks.onMouseUp, false); //soltar o clique

		document.addEventListener( 'touchstart', this.callbacks.onTouchStart, false );
		document.addEventListener( 'touchmove', this.callbacks.onTouchMove, false );
		document.addEventListener( 'touchend', this.callbacks.onTouchEnd, false );

		//quando o conteúdo html do renderer for selecionado
		container.addEventListener('selectstart', this.callbacks.onSelectStart, false);
	}
};


function QuadranteCirculo(raio, theta)
{
	this.mesh;
	this.geometry;
	this.material;
	this.raio = raio;
	this.theta = theta;

	var self = this;

	this.inicializar = function()
	{
		self.raio = raio;
		self.theta = theta;

		self.geometry = self.criarGeometria(self.theta);
		self.material = new THREE.MeshBasicMaterial( { color: 0xffffff } );
		self.mesh = new THREE.Mesh( self.geometry, self.material );
	};

	this.criarGeometria = function(theta)
	{
		var segments = parseInt(theta / 3);
		var thetaLength = ((Math.PI * 2) / 360) * theta;

		return new THREE.CircleGeometry( self.raio, segments, 0, thetaLength );
	};

	this.atualizar = function(theta)
	{
		self.theta = theta;

		self.geometry.dispose();
		self.geometry = this.criarGeometria(self.theta);
		self.mesh.geometry = self.geometry;
	};

	this.inicializar();
}

/**
4 questoes = 1 nivel
3 questoes = 2 nivel
2 questoes = 3 nivel
1 questoes = 4 nivel
*/
function Jogo()
{
	this.questoes;
	this.indice;
	this.level;
	this.qtdCertas;

	var self = this;

	this.desenhaQuestao = function()
	{
		$('#quantidadeCorreta').html(self.qtdCertas);
		$('#id-question').html(self.questoes[self.indice].id);
		$('#enunciation').html(self.questoes[self.indice].enunciation.replace("p/", "π"));
		self.resposta = self.questoes[self.indice].answer;

	}

	this.proximaQuestao = function()
	{
		self.indice += 1;
		this.desenhaQuestao();
	}

	this.buscaNivel = function()
	{
		var urlService = "http://circulo-trigonometrico.elasticbeanstalk.com/rest/level/" + self.level;

		$(document).ready(function() {
				$.ajax({
						url: urlService,
						async: false
				}).then(function(data) {
					 $('#id-level').html(data.id);
					 $('#level').html(data.description);
				});
		});
	}

	this.buscaQuestoes = function()
	{
		var urlService = "http://circulo-trigonometrico.elasticbeanstalk.com/rest/question/all/" + self.level;

		$(document).ready(function() {
				$.ajax({
						url: urlService,
						async: false
				}).then(function(data) {
					self.questoes = [];
					if(self.level == 1) {
						for(var i = 0; i < 4; i++) {
								var x = Math.floor((Math.random() * data.length));
								self.questoes[i] = data[x];
								data.splice(x, 1);
						}

					} else if(self.level == 2) {
						for(var i = 0; i < 3; i++) {
								var x = Math.floor((Math.random() * data.length));
								self.questoes[i] = data[x];
								data.splice(x, 1);
						}

					} else if(self.level == 3) {
						for(var i = 0; i < 2; i++) {
								var x = Math.floor((Math.random() * data.length));
								self.questoes[i] = data[x];
								data.splice(x, 1);
						}

					} else if(self.level == 4) {
						for(var i = 0; i < 1; i++) {
								var x = Math.floor((Math.random() * data.length));
								self.questoes[i] = data[x];
								data.splice(x, 1);
						}

					}
				});
		});
	}

	this.iniciaJogo = function()
	{
		this.indice = 0;
		this.level = 1;
		this.qtdCertas = 0;
	}

	this.escondeMensagens = function()
	{
		$('#acerto').hide();
		$('#erro').hide();
	}

	this.validaResposta = function(answer)
	{
		if(answer == self.questoes[self.indice].answer){
			$('#acerto').show();
			self.qtdCertas += 1;
		} else {
			$('#erro').show();
		}

		if(self.questoes.length == self.indice+1) {
			self.indice = 0;
			self.level += 1;
			$('#proxima').hide();
			$('#validar').hide();
			$('#proximoNivel').show();
		} else {
			$('#validar').hide();
			$('#proxima').show();
		}
	}

}

// Converts from degrees to radians.
Math.radians = function(degrees)
{
	return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function(radians)
{
	return radians * 180 / Math.PI;
};

Math.angulo = function(x, y)
{
	var theta = Math.atan2(y, x);
	var angle = Math.degrees(theta);

	if (angle > 0)
		return angle;
	else
		return 360 + angle;
};

vectorDeRaioAngulo = function(angulo, raio)
{
	var x = raio * Math.cos(Math.PI * angulo / 180.0);
	var y = raio * Math.sin(Math.PI * angulo / 180.0);

	return new THREE.Vector3(x, y, 0);
};
