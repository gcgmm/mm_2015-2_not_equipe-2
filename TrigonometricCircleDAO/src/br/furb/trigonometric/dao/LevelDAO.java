package br.furb.trigonometric.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.furb.trigonometric.bean.Level;
import br.furb.trigonometric.db.DBConnection;

public class LevelDAO {
	
	private static final String SELECT_LEVEL_DESC = "SELECT DESCRICAO FROM NIVEL WHERE ID = ?";

	private DBConnection dbConn;

	public LevelDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}

	/**
	 * Busca um n�vel conforme o identificador informado.
	 * 
	 * @param levelId - o identificador do n�vel a ser buscado na base.
	 * @return o n�vel.
	 * @throws SQLException caso de algum problema no acesso ao banco.
	 */
	public Level selectLevel(int levelId) throws SQLException {
		Level level = null;
		
		try (Connection conn = dbConn.getConexao();
				PreparedStatement pstm = conn.prepareStatement(SELECT_LEVEL_DESC)) {

			pstm.setInt(1, levelId);
			ResultSet rs = pstm.executeQuery();

			if (rs.next()) {
				String desc = rs.getString("DESCRICAO");
				level = new Level(levelId, desc);
			}
		}
		
		return level;
	}

}
