package br.furb.trigonometric.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.furb.trigonometric.bean.Question;
import br.furb.trigonometric.db.DBConnection;

public class QuestionDAO {

	private static final String SELECT_QUESTION = "SELECT Q.ID ID, Q.ENUNCIADO ENUM, Q.RESPOSTA RESP FROM QUESTAO Q, NIVEL_QUESTAO NQ WHERE NQ.ID_NIVEL = ? AND NQ.ID_QUESTAO = Q.ID";
	private static final String SELECT_QUESTION_FROM_LEVEL = "SELECT Q.ID ID, Q.ENUNCIADO ENUM, Q.RESPOSTA RESP FROM QUESTAO Q, NIVEL_QUESTAO NQ WHERE NQ.ID_NIVEL = ? AND NQ.ID_QUESTAO = Q.ID AND Q.ID = ?";

	private DBConnection dbConn;

	public QuestionDAO(DBConnection dbConn) {
		this.dbConn = dbConn;
	}

	public List<Question> selectAllQuestionsFromLevel(int levelId) throws SQLException {
		List<Question> questions = new ArrayList<>();
		
		try (Connection conn = dbConn.getConexao();
				PreparedStatement pstm = conn.prepareStatement(SELECT_QUESTION)) {

			pstm.setInt(1, levelId);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("ID");
				String enunciation = rs.getString("ENUM");
				short answer = rs.getShort("RESP");

				questions.add(new Question(id, enunciation, answer));
			}
		}
		
		return questions;
	}
	
	public Question selectQuestion(int levelId, int questionId) throws SQLException {
		Question question = null;
		
		try (Connection conn = dbConn.getConexao();
				PreparedStatement pstm = conn.prepareStatement(SELECT_QUESTION_FROM_LEVEL)) {

			pstm.setInt(1, levelId);
			pstm.setInt(2, questionId);
			ResultSet rs = pstm.executeQuery();

			if (rs.next()) {
				int id = rs.getInt("ID");
				String enunciation = rs.getString("ENUM");
				short answer = rs.getShort("RESP");

				question = new Question(id, enunciation, answer);
			}
		}
		
		return question;
	}
}
