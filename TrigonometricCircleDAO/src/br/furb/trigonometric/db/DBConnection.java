package br.furb.trigonometric.db;

import java.sql.Connection;
import java.sql.SQLException;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class DBConnection {

	// Constantes de erros da base de dados
	public static final int CHAVE_UNICA_VIOLADA = 2627;

	// Create a variable for the connection string.
	private final String servidor = "54.207.45.112";
	private final String baseDeDados = "RECEITAS";
	private final String usuario = "furb";
	private final String senha = "sql@2015";

	private Connection conexao;

	public DBConnection() {
		conexao = null;
	}

	public void abrirConexao() throws SQLException {
		if (conexao == null) {
			SQLServerDataSource ds = new SQLServerDataSource();
			ds.setIntegratedSecurity(false);
			ds.setServerName(servidor);
			ds.setPortNumber(1433);
			ds.setDatabaseName(baseDeDados);
			ds.setUser(usuario);
			ds.setPassword(senha);

			conexao = ds.getConnection();
		}
	}

	public void fecharConexao() throws SQLException {
		if (conexao != null) {
			conexao.close();
			conexao = null;
		}
	}

	public Connection getConexao() {
		return conexao;
	}

}
