create table questao (
	ID integer,
	ENUNCIADO varchar(50),
	RESPOSTA varchar(50),
	primary key (ID)
);

create table nivel (
	ID integer,
	DESCRICAO varchar(120),
	primary key (ID)
);

create table nivel_questao (
	ID_NIVEL integer,
	ID_QUESTAO integer,
	primary key (ID_NIVEL, ID_QUESTAO)
);

alter table NIVEL_QUESTAO add constraint FK_QUESTAO foreign key (ID_QUESTAO) references QUESTAO (ID);
alter table NIVEL_QUESTAO add constraint FK_NIVEL foreign key (ID_NIVEL) references NIVEL (ID);

insert into nivel values(1, 'Faça a redução de quadrante');
insert into nivel values(2, 'Calcule o valor do ângulo em cada caso e identifique a resposta no quadrante');
insert into nivel values(3, 'Calcular, por redução ao primeiro quadrante');
insert into nivel values(4, 'Calcule a primeira determinação positiva do conjunto de arcos de mesma extremidade que o arco A de medida igual a');

--nivel 1 
insert into questao values(1, '45° no terceiro quadrante', 225);
insert into questao values(2, '120° no quarto quadrante', 300);
insert into questao values(3, '210° no primeiro quadrante', 30);
insert into questao values(4, '30° no quarto quadrante', 330);
insert into questao values(5, '150° no primeiro quadrante', 60);
insert into questao values(6, '315° no primeiro quadrante', 45);
insert into questao values(7, '330° no segundo quadrante', 150);
insert into questao values(8, '210° no segundo quadrante', 120);
insert into questao values(9, 'π/3 no primeiro quadrante', 240);
insert into questao values(10, '7π/6 no terceiro quadrante', 30);
insert into questao values(11, '5π/3 no quarto quadrante', 300);
insert into questao values(12, '7π/6 no segundo quadrante', 210);
insert into questao values(13, '4π/6 no quarto quadrante', 120);
insert into questao values(14, 'π/2 no terceiro quadrante', 270);
insert into questao values(15, '5π/4 no primeiro quadrante', 45);
insert into questao values(16, '3π/4 no quarto quadrante', 315);

--nivel 2
insert into questao values(17, '45° + 122° - 107°', 60);
insert into questao values(18, '360° - 70° * 2°', 220);
insert into questao values(19, '10° / 2° + 40°', 45);
insert into questao values(20, '90° - 20° * 3°', 30);
insert into questao values(21, '25° + 25° * 5°', 150);
insert into questao values(22, '30° - 40° + 60° * 4°', 230);

--nivel 3
insert into questao values(23, 'sen 150°', 30);
insert into questao values(24, 'sen 225°', 45);
insert into questao values(25, 'sen 330°', 30);
insert into questao values(26, 'sen 3π/4', 45);
insert into questao values(27, 'cos 11π/6', 30);

--nivel 4
insert into questao values(28, ' 810°', 90);
insert into questao values(29, '-1820°', 340);
--insert into questao values(30, '810°', 90);

--nivel 1
insert into nivel_questao values(1,1);
insert into nivel_questao values(1,2);
insert into nivel_questao values(1,3);
insert into nivel_questao values(1,4);
insert into nivel_questao values(1,5);
insert into nivel_questao values(1,6);
insert into nivel_questao values(1,7);
insert into nivel_questao values(1,8);
insert into nivel_questao values(1,9);
insert into nivel_questao values(1,10);
insert into nivel_questao values(1,11);
insert into nivel_questao values(1,12);
insert into nivel_questao values(1,13);
insert into nivel_questao values(1,14);
insert into nivel_questao values(1,15);
insert into nivel_questao values(1,16);

--nivel 2
insert into nivel_questao values(2,17);
insert into nivel_questao values(2,18);
insert into nivel_questao values(2,19);
insert into nivel_questao values(2,20);
insert into nivel_questao values(2,21);
insert into nivel_questao values(2,22);

--nivel 3
insert into nivel_questao values(3,23);
insert into nivel_questao values(3,24);
insert into nivel_questao values(3,25);
insert into nivel_questao values(3,26);
insert into nivel_questao values(3,27);

--nivel 4
insert into nivel_questao values(4,28);
insert into nivel_questao values(4,29);
insert into nivel_questao values(4,30);