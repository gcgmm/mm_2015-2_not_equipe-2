package br.furb.trigonometric.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa um n�vel do jogo.
 */
public class Level {
	
	private int id;
	private String description;
	
	public Level(int id, String description, List<Question> questions) {
		this.id = id;
		this.description = description;
	}
	
	public Level(int id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
