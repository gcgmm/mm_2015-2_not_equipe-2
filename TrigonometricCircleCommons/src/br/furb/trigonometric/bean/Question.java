package br.furb.trigonometric.bean;

/**
 * Representa uma quest�o.
 */
public class Question {
	
	private int id;
	private String enunciation;
	private short answer;
	
	public Question(int id, String enunciation, short answer) {
		this.id = id;
		this.enunciation = enunciation;
		this.answer = answer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEnunciation() {
		return enunciation;
	}

	public void setEnunciation(String enunciation) {
		this.enunciation = enunciation;
	}

	public short getAnswer() {
		return answer;
	}

	public void setAnswer(short answer) {
		this.answer = answer;
	}
	
}
