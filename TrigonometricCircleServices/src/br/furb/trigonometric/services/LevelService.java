package br.furb.trigonometric.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.furb.trigonometric.bean.Level;
import br.furb.trigonometric.dao.LevelDAO;
import br.furb.trigonometric.db.DBConnection;

@Path("level")
public class LevelService {

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response findLevel(@PathParam("id") int id) {
		Level level = null;
		
		try {
			DBConnection dbConn = new DBConnection();
			dbConn.abrirConexao();
			try {
				level = new LevelDAO(dbConn).selectLevel(id);
			} finally {
				dbConn.fecharConexao();
			}

			if (level == null) {
				return Response.noContent().build();
			}
			return Response.ok(level).build();
		} catch (SQLException sqlEx) {
			return Response.serverError().entity("Erro de SQL").build();
		}
	}

}
