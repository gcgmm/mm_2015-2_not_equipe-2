package br.furb.trigonometric.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.furb.trigonometric.bean.Question;
import br.furb.trigonometric.dao.QuestionDAO;
import br.furb.trigonometric.db.DBConnection;

@Path("question")
public class QuestionService {
	
	@GET
	@Path("{levelId}/{questionId}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response findQuestionFromLevel(@PathParam("levelId") int levelId, @PathParam("questionId") int questionId) {
		Question question  = null;
		
		try {
			DBConnection dbConn = new DBConnection();
			dbConn.abrirConexao();
			try {
				question = new QuestionDAO(dbConn).selectQuestion(levelId, questionId);
			} finally {
				dbConn.fecharConexao();
			}

			if (question == null) {
				return Response.noContent().build();
			}
			return Response.ok(question).build();
		} catch (SQLException sqlEx) {
			return Response.serverError().entity("Erro de SQL").build();
		}
	}
	
	@GET
	@Path("all/{levelId}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response findAllQuestionsFromLevel(@PathParam("levelId") int levelId) {
		List<Question> questions = new ArrayList<>();
		
		try {
			DBConnection dbConn = new DBConnection();
			dbConn.abrirConexao();
			try {
				questions = new QuestionDAO(dbConn).selectAllQuestionsFromLevel(levelId);
			} finally {
				dbConn.fecharConexao();
			}

			if (questions == null) {
				return Response.noContent().build();
			}
			return Response.ok(questions).build();
		} catch (SQLException sqlEx) {
			return Response.serverError().entity("Erro de SQL").build();
		}
	}

}
