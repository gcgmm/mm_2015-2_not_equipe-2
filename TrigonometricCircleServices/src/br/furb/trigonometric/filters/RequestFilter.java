package br.furb.trigonometric.filters;

import javax.ws.rs.WebApplicationException;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

public class RequestFilter implements ContainerRequestFilter {
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Override
	public ContainerRequest filter(ContainerRequest request)
			throws WebApplicationException {

		return request;
	}

}